;; -*- lexical-binding: t; -*-

(defun meff/server-quit ()
  "Save buffers, Quit, and Shutdown (kill) server"
  (interactive)
  (save-some-buffers)
  (kill-emacs))

(defun meff/copy-file-and-line-number ()
  "Copy the current cursor line number and filepath, to use
in things like org-insert-link"
  (interactive)
  (let ((fullpath (format "file:%s::%d"
			  (buffer-file-name)
			  (line-number-at-pos))))
    (kill-new fullpath)
    (message "copyied: %s" fullpath)))

(defun +eshell/eshell-new ()
  "Open multiple eshell buffers"
  (interactive)
  (eshell 'N))

(defun +ivy/search-in-project (query extra-rg-args)
  "Conduct a file search using ripgrep.

:query STRING
  Determines the initial input to search for.
:extra-rg-args
  Extra arguments for ripgrep"
  (interactive)
  (unless (executable-find "rg")
    (user-error "Couldn't find ripgrep in your PATH"))
  (unless (boundp 'projectile-project-name)
    (user-error "Not in a project"))
  (if (boundp 'projectile-project-root)
      (counsel-rg query projectile-project-root extra-rg-args)
    (user-error
     (format "Project name is: {%s} but there is no projectile root. Projectile error?"
	     projectile-project-name))))

(defun meff/maybe-url-from-region-or-current-kill ()
  (let* ((the-active-region (if (region-active-p)
				(buffer-substring (region-beginning) (region-end))
			      nil))
	 (the-current-kill (ignore-errors (current-kill 0 t))))
    (cond
     ((and (stringp the-current-kill) (s-match "^https?://.*" the-current-kill))
      the-current-kill)
     ((and (stringp the-active-region) (s-match "^https?://.*" the-active-region))
      the-active-region)
     (t nil))))

(defun meff/url-to-bibtex-string (url title key)
  (format "@misc{%s,\n  title = {{%s}},\n  howpublished = {\\url{%s}},\n}" key title url))

(defun meff/url-to-bibtex (url title key &optional bibfile)
  (interactive (list (read-string "URL: " (meff/maybe-url-from-region-or-current-kill))
		     (read-string "Title: ")
		     (read-string "Keyword: ")))
  (unless bibfile
    (setq bibfile (completing-read "Bibfile: "
				   (if (functionp 'org-ref-possible-bibfiles)
				       (org-ref-possible-bibfiles)
				     nil)))
  (save-window-excursion
    (with-current-buffer
	(find-file-noselect bibfile)
      (goto-char (point-min))
      (cond ((word-search-forward key nil t)
	     (message "There is already an entry with keyword %s" key))
	    ((word-search-forward url nil t)
	     (message "This URL is already registered"))
	    (t (goto-char (point-max))
	       (when (not (looking-back "\n\n" (min 3 (point))))
		 (insert "\n\n"))
	       (insert (meff/url-to-bibtex-string url title key))
	       (save-buffer)))))))

(defun meff/mpv-set-volume (socket-file volume)
  (defcustom socket socket-file
    "no me rompas los huevos"
    :type 'string)
  (let* ((process (make-network-process :name "elsocket"
					:family 'local
					:service socket)))
    (process-send-string process
			 (format "{\"command\": [\"%s\", \"%s\", %d]}"
				 "set_property"
				 "volume"
				 volume))))

(defun meff/bongo-mpv-set-volume (volume)
  (interactive "NVolume: ")
  (meff/mpv-set-volume "/tmp/bongo-mpv.socket" volume))

;; Stealed from https://emacs.stackexchange.com/a/47930
(defun c-forward-to-argsep ()
  "Move to the end of the current c function argument.
Returns point."
  (interactive)
  (while (progn (comment-forward most-positive-fixnum)
        (looking-at "[^,)]"))
    (forward-sexp))
  (point))

(defun c-backward-to-argsep ()
  "Move to the beginning of the current c function argument.
Returns point."
  (interactive)
  (let ((pt (point))
    cur)
    (up-list -1)
    (forward-char)
    (while (progn
         (setq cur (point))
         (> pt (c-forward-to-argsep)))
      (forward-char))
    (goto-char cur)))

(defun c-transpose-args ()
  "Transpose two arguments of a c-function.
The first arg is the one with point in it."
  (interactive)
  (let* ((pt (point))
     (b (c-backward-to-argsep))
     (sep (progn (goto-char pt)
             (c-forward-to-argsep)))
     (e (progn
          (unless (looking-at ",")
        (user-error "Argument separator not found"))
          (forward-char)
          (c-forward-to-argsep)))
     (ws-first (buffer-substring-no-properties
            (goto-char b)
            (progn (skip-chars-forward "[[:space:]\n]")
               (point))
            ))
     (first (buffer-substring-no-properties (point) sep))
     (ws-second (buffer-substring-no-properties
             (goto-char (1+ sep))
             (progn (skip-chars-forward "[[:space:]\n]")
                (point))))
     (second (buffer-substring-no-properties (point) e)))
    (delete-region b e)
    (insert ws-first second "," ws-second first)))

(defun reverse-region-str (beg end)
  (interactive "r")
  (let ((reversed (reverse (buffer-substring beg end))))
    (delete-region beg end)
    (insert reversed)))

(defun turn-on-show-trailing-whitespace ()
  (setq show-trailing-whitespace t))

(defun cppreference (search)
  (interactive "sSearch: ")
  (eww (format "%s%s" "https://duckduckgo.com/?sites=cppreference.com&q=" search)))



;;; Define projectile-alternative-compile; very similar to
;;; projectile-test-compile

(defvar meff/projectile-project-alternative-compilation-cmd nil
  "The command to use with `meff/projectile-alternative-compile'.
It takes precedence over the default command for the project type when set.
Should be set via .dir-locals.el.")

(defun meff/projectile-alternative-compile (arg)
  "Run project alternative compile command.

Normally you'll be prompted for a compilation command, unless
variable `compilation-read-command'.  You can force the prompt
with a prefix ARG."
  (interactive "P")
  (projectile--run-project-cmd meff/projectile-project-alternative-compilation-cmd nil
			       :show-prompt arg
			       :prompt-prefix "Alternative compilation command: "
			       :save-buffers t))


;;; Define projectile-project-format; Format the whole project

(defvar meff/projectile-project-format-cmd nil
  "The command to use with `meff/projectile-project-format'.
It takes precedence over the default command for the project type when set.
Should be set via .dir-locals.el.")

(defun meff/projectile-project-format (arg)
  "Format the whole project.

Normally you'll be prompted for a compilation command, unless
variable `compilation-read-command'.  You can force the prompt
with a prefix ARG."
  (interactive "P")
  (projectile--run-project-cmd meff/projectile-project-format-cmd nil
			       :show-prompt arg
			       :prompt-prefix "Format command: "
			       :save-buffers t))

(defun meff/org-to-pdf-postprocess-reformat-caption ()
  (goto-char (point-min))
  ;; 1 - line before
  ;; 2 - start of caption
  ;; 3 - label
  ;; 4 - end of caption
  ;; 5 - following line
  (while (re-search-forward
	  "\\(\\\\end{minted}\n\\)\\(\\\\caption{\\)\\(\\\\label{[^}]+}\\)\\(.*\\)\\(\n\\\\end{code}\\)"
	  nil
	  t)
    (replace-match "\\1\\2\\4\n\\3\\5")))

(defun meff/replace-all-in-buffer (from-string rep)
  (goto-char (point-min))
  (while (re-search-forward from-string nil t)
    (replace-match rep)))

(defun meff/org-to-pdf-postprocess-replace-begin-listing ()
  (meff/replace-all-in-buffer "\\\\begin{listing}.*" "\\\\begin{code}"))

(defun meff/org-to-pdf-postprocess-replace-end-listing ()
  (meff/replace-all-in-buffer "\\\\end{listing}" "\\\\end{code}"))

(defun meff/org-to-pdf-postprocess ()
  (meff/org-to-pdf-postprocess-replace-begin-listing)
  (meff/org-to-pdf-postprocess-replace-end-listing)
  (meff/org-to-pdf-postprocess-reformat-caption))

(defun meff/org-to-pdf ()
  "Run org-latex-export-to-pdf postprocessing it"
  (interactive)
  (let ((outfile (org-export-output-file-name ".tex" nil)))
    (org-latex-export-to-latex)
    (f-write-text (with-temp-buffer
		    (insert-file-contents outfile)
		    (meff/org-to-pdf-postprocess)
		    (buffer-string))
		  'utf-8
		  outfile)
    (compile (format "pdflatex -shell-escape %s && pdflatex -shell-escape %s" outfile outfile))))

(defvar meff/python-utest-template "import unittest
from unittest import mock

from %s import %s


class Test%s(unittest.TestCase):")

;; (defvar meff/python-utest-dir (projectile-project-root))

(defun meff/python-utest (class tests-dir)
  ""
  (interactive (list (read-string "Class to test: " (current-word))
		     (read-string "Test file: " meff/python-utest-dir)))
  (let ((output-file (format "%s/%s%s.py" tests-dir "test" (meff/camel-case-to-snake-case class))))
    (f-write-text
     (format meff/python-utest-template (file-name-base) class class)
     'utf-8
     output-file)
    (find-file output-file)))

(defun meff/camel-case-to-snake-case (word)
  (message (replace-regexp-in-string "[A-Z]"
				     (lambda (char)
				       (format "_%s" (downcase char)))
				     word
				     't)))

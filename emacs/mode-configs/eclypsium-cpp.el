(message "Loading eclypsium-cpp")
(defconst endpoint-style
  '((c-basic-offset         . 4)
    (c-offsets-alist        . ((substatement-open 0)
			       (statement-cont 0)
			       (defun-open 0)
			       (defun-block-intro +)
			       (func-decl-cont 0)
			       (class-open 0)
			       (inline-open 0)
			       (arglist-cont-nonempty
				(c-lineup-gcc-asm-reg c-lineup-arglist))
			       (case-label +)))
    (c-hanging-braces-alist . ((substatement-open after)
			       (brace-list-open)
			       (brace-entry-open))) ; not sure what this is
    (indent-tabs-mode       . nil)
    (show-trailing-spaces   . t)
    (c-backslash-max-column . 100))
  "Endpoint c/c++ style")

(c-add-style "endpoint" endpoint-style)

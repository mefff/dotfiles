;; -*- lexical-binding: t; -*-

;; suspend gc
(defvar last-file-name-handler-alist file-name-handler-alist)
(setq gc-cons-threshold 402653184
      gc-cons-percentage 0.6
      file-name-handler-alist nil)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

(setq emacs-dir "/home/meff/.emacs.d/")
(load (concat emacs-dir "util.el") nil t) ;; load util.el
(setq custom-file (concat emacs-dir "custom.el"))
(load custom-file nil t)

(setq inhibit-splash-screen t) ;; no splash
(savehist-mode 1)
(menu-bar-mode -1)
(tool-bar-mode -1)
(set-scroll-bar-mode nil)
(setq vc-follow-symlinks t) ;; always follow links
(setq scroll-conservatively 100) ;; vim-like scroll
(defalias 'yes-or-no-p 'y-or-n-p)
(setq browse-url-browser-function 'browse-url-firefox)
(setq-default case-fold-search nil) ;; DO NOT IGNORE CASE by default
(electric-pair-mode)
(setq backup-directory-alist
      `(("." .
	 ,(concat emacs-dir "autosaves")))) ;; autosaves
(setq history-delete-duplicates t)
(setq history-length 1000)

(global-set-key (kbd "C-x C-z") nil) ;; remove minimize keybind

;; use-package init
(eval (when (not (package-installed-p 'use-package))
	(package-refresh-contents)
	(package-install 'use-package))
      (require 'use-package))

;; (use-package benchmark-init
;;   :ensure t
;;   :config
;;   ;; To disable collection of benchmark data after init is done.
;;   (add-hook 'after-init-hook 'benchmark-init/deactivate))

(use-package dired
  :bind (("C-x j" . dired-jump)
	 ("C-x J" . (lambda () (interactive) (dired-other-window ".")))
	 :map dired-mode-map
	      ("<C-return>" . dired-find-file-other-window)
	      ("C-c n" . make-directory))
  :config
  (setq dired-listing-switches "-alh"))

(use-package dired-x
  :after dired
  :config
  (setq dired-omit-extensions (cons ".o.cmd" dired-omit-extensions)))

(use-package eshell
  :bind
  ("C-x e" . eshell)
  ("C-x E" . +eshell/eshell-new))

(use-package window
  :custom
  (display-buffer-alist
   '(("\\*e?shell.*\\*\\|\\*.*vterm.*\\*"
      (display-buffer-in-side-window)
      (window-height . 0.27)
      (side . bottom)
      (slot . -1))
     ("\\*\\(Warnings\\|compilation\\|grep\\|ripgrep-search\\|[Hh]elp\\|Messages\\|xref\\)\\*"
      (display-buffer-in-side-window)
      (window-height . 0.27)
      (side . bottom)
      (slot . 1))))
  :bind (("C-c t" . window-toggle-side-windows)))

(use-package doom-themes
  :ensure t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
	doom-themes-enable-italic t) ; if nil, italics is universally disabled
  (load-theme 'doom-one-light t)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

(use-package doom-modeline :ensure t
  :init (doom-modeline-mode 1)
  :custom
  (doom-modeline-icon nil)
  (doom-modeline-height 10)
  (doom-modeline-vcs-max-length 30)
  (doom-modeline-buffer-file-name-style 'relative-from-project))

(use-package evil
  :ensure t
  :init
  (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-undo-system 'undo-redo)
  :config
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))

(use-package projectile
  :ensure t
  :bind-keymap ("C-c p" . projectile-command-map)
  :config
  (setq projectile-project-search-path '("~/eclypsium/" "~/builds/")
	projectile-ignored-project-function '+projectile/ignore-function
	projectile-per-project-compilation-buffer t)
  (define-key projectile-command-map (kbd "A") #'meff/projectile-alternative-compile)
  (define-key projectile-command-map (kbd "TAB") #'meff/projectile-project-format)
  (projectile-mode 1))

(use-package which-key :ensure t
  :config
  (setq which-key-max-display-columns nil)
  (setq which-key-max-description-length nil)
  (which-key-mode)
  (which-key-setup-minibuffer))

(use-package wgrep :ensure t)

(use-package counsel :ensure t)

(use-package ivy :ensure t
  :bind ("C-c *" . (lambda ()
		     (interactive)
		     (+ivy/search-in-project "" "")))
  :config
  (setq ivy-on-del-error-function nil)
  (ivy-mode 1))

(use-package prescient :ensure t
  :config
  (setq prescient-filter-method '(literal))
  (prescient-persist-mode))

(use-package ivy-prescient :ensure t :after ivy :after counsel
  :config
  (ivy-prescient-mode t))

(use-package ripgrep :ensure t
  :config
  (setq xref-search-program 'ripgrep))

(use-package org :ensure t
  :config
  (setq org-todo-keywords
	'((sequence "TODO" "MAYBE" "|" "DONE" "HOLD")))
  (add-hook 'org-mode-hook 'auto-fill-mode))

(use-package ox-latex :after org
  :config
  ;; Multiple pdflatex for bibtex to work lmao
  ;;
  ;; Need the `-shell-escape' for minted (latex package for code
  ;; formating)
  (setq org-latex-src-block-backend 'minted
	org-latex-pdf-process
	'("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
	  "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f")))

(use-package ob-mongo :after org :ensure t)

(use-package ob :after org
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (C . t)
     (shell . t)
     (dot . t)
     (python . t)
     (mongo . t))))

(use-package org-ref :ensure t :after org
  :init
  (require 'ivy-bibtex)
  (require 'org-ref-ivy)
  :config (setq org-ref-insert-link-function 'org-ref-insert-link-hydra/body
		org-ref-insert-cite-function 'org-ref-cite-insert-ivy
		org-ref-insert-label-function 'org-ref-insert-label-link
		org-ref-insert-ref-function 'org-ref-insert-ref-link
		org-ref-cite-onclick-function (lambda (_) (org-ref-citation-hydra/body))

		bibtex-completion-bibliography '("~/docs/references.bib")
		bibtex-completion-library-path '("~/docs")))

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "~/notes"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  (org-roam-db-autosync-mode))

(use-package ivy-bibtex :ensure t)

(use-package magit :ensure t :defer 2
  :bind (("C-c g c" . magit-checkout)
	 ("C-c g b" . magit-blame-addition)
	 ("C-c g f" . magit-find-file)
	 ("C-c g F" . magit-find-file-other-window)
	 ("C-c g h" . vc-revision-other-window))
  :bind (:map
	 magit-mode-map
	 ("C-<return>" . magit-diff-visit-file-other-window)))

(use-package virtualenvwrapper :ensure t :defer 2)

(use-package erc :defer 2
  :init
  (setq erc-server "irc.libera.chat"
	erc-port 6667
	erc-nick "meff_"
	erc-fill-function 'erc-fill-static
	erc-fill-static-center 22
	erc-autojoin-timing 'ident
	erc-track-exclude-types '("JOIN" "NICK" "PART" "QUIT")
	erc-lurker-hide-list '("JOIN" "PART" "QUIT"))
  (use-package erc-services
    :config
    (erc-services-mode 1)
    (add-to-list 'erc-modules 'scrolltobottom))
  (use-package erc-hl-nicks
    :ensure t
    :config
    (erc-hl-nicks-mode)))

(use-package haskell-mode :ensure t)

(use-package rmsbolt :ensure t)

(use-package doc-view
  :init (add-hook 'doc-view-mode-hook 'auto-revert-mode)
  :config (setq doc-view-continuous 't))

(use-package ispell
  :config
  (setq ispell-program-name "/usr/bin/hunspell")
  ; (ispell-change-dictionary "es_AR")
  )

(use-package empv
  :if (file-directory-p "~/builds/empv.el")
  :load-path "~/builds/empv.el"
  :bind (("C-c m c" . empv-toggle)
	 ("C-c m P" . empv-playlist-select)
	 ("C-c m n" . empv-playlist-next)
	 ("C-c m p" . empv-playlist-prev)
	 ("C-c m a" . empv-youtube)
	 ("C-c m s" . empv-display-current)
	 ("C-c m v" . empv-set-volume)
	 ("C-c m -" . (lambda () (interactive) (empv-change-vol -5)))
	 ("C-c m +" . (lambda () (interactive) (empv-change-vol 5))))
  :config (setq empv-invidious-instance "https://invidious.fdn.fr/api/v1"))

(use-package typit :ensure t)

(use-package cc
  :load-path (lambda () (concat emacs-dir "mode-configs"))
  :load-path "/usr/share/emacs/site-lisp/clang-format-11"
  :init
  (load "eclypsium-cpp.el")
  (load "kernel-standard.el")
  (load "clang-format.el")
  :bind (:map
	 c-mode-base-map
	 ("C-c c" . compile)
	 ([C-M-tab] . clang-format-region)
	 ("C-c M-t" . c-transpose-args)))

(use-package python)

;; (use-package lsp-mode :ensure t
;;   ;; :custom
;;   ;; (lsp-diagnostics-provider :none)
;;   :hook ((c-mode . lsp-deferred)
;; 	 (c++-mode . lsp-deferred)
;; 	 (python-mode . lsp-deferred)
;; 	 (lsp-mode . lsp-enable-which-key-integration)
;; 	 ;;; hack to actually disable lsp-headerline-breadcrumb-enable
;; 	 (lsp-mode . (lambda()
;; 		       (setq lsp-headerline-breadcrumb-enable nil))))
;;   :commands lsp lsp-deferred)

;; (use-package dap-mode :ensure t)
;; (use-package dap-cpptools
;;   :after dap-mode)

(use-package compile
  :custom
  (compilation-scroll-output t))

(use-package eww
  :bind (:map eww-mode-map
	      ("<C-return>" . eww-open-in-new-buffer)))

(use-package rainbow-delimiters :ensure t)

(use-package origami :ensure t)

(use-package ansi-color
  :config
  ;; Colorize compilation buffer
  (add-hook 'compilation-filter-hook
	    #'(lambda ()
		(ansi-color-apply-on-region compilation-filter-start
					    (point-max)))))

(use-package flymake-json :ensure t
  :config
  (add-hook 'json-mode 'flymake-json-load))

;; emacs bindings
(global-set-key (kbd "C-x C-;") 'comment-or-uncomment-region)
(global-set-key (kbd "C-x x") 'meff/server-quit)
(global-set-key (kbd "C-x l") 'meff/copy-file-and-line-number)
(evil-global-set-key 'normal "gr" #'(lambda ()
				      (interactive)
				      (revert-buffer nil 't)))
(global-set-key (kbd "M-[") 'previous-error)
(global-set-key (kbd "M-]") 'next-error)

;; restore gc
(setq gc-cons-threshold 16777216
      gc-cons-percentage 0.1
      file-name-handler-alist last-file-name-handler-alist)

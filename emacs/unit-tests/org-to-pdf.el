(load-file "../util.el")

(defun meff/run-test (input expected fn-to-test assertion &optional test-case)
  (message "Running test %s" (if test-case test-case ""))
  (let ((output (with-temp-buffer
		  (insert input)
		  (funcall fn-to-test)
		  (buffer-string))))
    (if (funcall assertion output expected)
	(message "OK")
      (message "FAILED\nExpected:\n%s\nActual:\n%s" expected output))))

;; Test - meff/org-to-pdf-postprocess-replace-begin-listing
(meff/run-test
 "\\begin{listing}[htbp]"
 "\\begin{code}"
 'meff/org-to-pdf-postprocess-replace-begin-listing
 'string=
 "meff/org-to-pdf-postprocess-replace-begin-listing/ok")

(meff/run-test
 "\\begin{listing}"
 "\\begin{code}"
 'meff/org-to-pdf-postprocess-replace-begin-listing
 'string=
 "meff/org-to-pdf-postprocess-replace-begin-listing/ok-no-htbp")

(meff/run-test
 "\\begin{listing}
\\end{listing}
\\begin{listing}
\\end{listing}"
 "\\begin{code}
\\end{listing}
\\begin{code}
\\end{listing}"
 'meff/org-to-pdf-postprocess-replace-begin-listing
 'string=
 "meff/org-to-pdf-postprocess-replace-begin-listing/ok-multiple")

(meff/run-test
 "\\end{listing}"
 "\\end{code}"
 'meff/org-to-pdf-postprocess-replace-end-listing
 'string=
 "meff/org-to-pdf-postprocess-replace-end-listing/ok")

(meff/run-test
 "\\end{listing}extra-text"
 "\\end{code}extra-text"
 'meff/org-to-pdf-postprocess-replace-end-listing
 'string=
 "meff/org-to-pdf-postprocess-replace-end-listing/no-change-extra-text-trailing")

(meff/run-test
 "\\begin{listing}
\\end{listing}
\\begin{listing}
\\end{listing}"
 "\\begin{listing}
\\end{code}
\\begin{listing}
\\end{code}"
 'meff/org-to-pdf-postprocess-replace-end-listing
 'string=
 "meff/org-to-pdf-postprocess-replace-end-listing/ok-multiple")

(meff/run-test
 "\\begin{listing}
\\end{listing}
\\begin{listing}
\\end{listing}"
 "\\begin{code}
\\end{code}
\\begin{code}
\\end{code}"
 (lambda () (list (meff/org-to-pdf-postprocess-replace-begin-listing)
		  (meff/org-to-pdf-postprocess-replace-end-listing)))
 'string=)

(meff/run-test
 "preambulo
\\end{minted}
\\caption{\\label{label-1}Caption \\texttt{content}.}
\\end{code}
postambulo"
 "preambulo
\\end{minted}
\\caption{Caption \\texttt{content}.}
\\label{label-1}
\\end{code}
postambulo"
 'meff/org-to-pdf-postprocess-reformat-caption
 'string=
 "meff/org-to-pdf-postprocess-reformat-caption/ok-simple")

(meff/run-test
 "preambulo
\\end{minted}
\\caption{\\label{label-1}Caption \\texttt{content}.}
\\end{code}
postambulo
text
\\end{minted}
\\caption{\\label{label-2}Other caption}
\\end{code}"
 "preambulo
\\end{minted}
\\caption{Caption \\texttt{content}.}
\\label{label-1}
\\end{code}
postambulo
text
\\end{minted}
\\caption{Other caption}
\\label{label-2}
\\end{code}"
 'meff/org-to-pdf-postprocess-reformat-caption
 'string=
 "meff/org-to-pdf-postprocess-reformat-caption/ok-multiple")

(let ((input (with-temp-buffer
	       (insert-file-contents "test-1.tex")
	       (buffer-string)))
      (expected (with-temp-buffer
		  (insert-file-contents "test-1.expected.tex")
		  (buffer-string))))
  (meff/run-test
   input
   expected
   'meff/org-to-pdf-postprocess
   'string=
   "meff/org-to-pdf-postprocess"))

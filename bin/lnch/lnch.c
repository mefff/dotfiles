#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv) {
  int pid = fork();
  char **args = &argv[1];
  // run the program as a child and disown it
  if (pid == 0) {
    execvp(argv[1], args);
  } else {
    setpgid(pid, 0);
    return 0;
  }
}

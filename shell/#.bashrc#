### prompt
git-branch() {
  branch=$(git branch 2>/dev/null| grep '*' | cut -d ' ' -f 2- | sed '(/d')
  [[ $branch ]] && branch=" (git:$branch)"
  printf "%s" "$branch"
}
# ANSI color codes
RS="\[\033[0m\]"    # reset
FCYN="\[\033[36m\]" # foreground cyan
export PS1="$FCYN\w$RS\$(git-branch) ─ "

### history
export HISTSIZE=
export HISTFILESIZE=
export HISTCONTROL="ignoreboth:erasedups"

### completion
[[ -f /etc/bash_completion ]] && . /etc/bash_completion

### alias
alias ls="ls --color"
alias grep="grep --color"
alias doom="prboom-plus -iwad ~/games/doom-wads/DOOM.WAD"
alias doom2="prboom-plus -iwad ~/games/doom-wads/DOOM2.WAD"
alias doom-tnt="prboom-plus -iwad ~/games/doom-wads/TNT.WAD"
alias shutdown="/sbin/poweroff"
alias reboot="/sbin/reboot"
alias glog="git log --decorate --oneline --graph --all"
alias r="ranger"
#alias e="emacs . 1,2> /dev/null & disown"
alias e="emacs --no-window-system"
alias tcd="st 2>/dev/null & disown"
alias add_tor="transmission-remote -a"
alias vim="nvim"
alias rtags="ctags --options=/home/meff/.vim/plugged/rust.vim/ctags/rust.ctags --languages=Rust -R src"

# power
alias zzz="sudo zzz"
alias shutdown="sudo shutdown -h"
alias reboot="sudo reboot"

# pdf
pdf() {
  zathura "$@" & disown > /dev/null
}


### fzf

# open vim with fzf
ef() { emacs --no-window-system $(fzf); }

# cd to facu/*
cdf() {
  ls=$(which ls)
  cd ~/facu/"$($ls ~/facu | fzf)"
}

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export FZF_DEFAULT_COMMAND='rg --files --hidden --follow'
export FZF_DEFAULT_OPTS="--layout=reverse"

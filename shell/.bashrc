### prompt
git-branch() {
  branch=$(git branch 2>/dev/null| grep '*' | cut -d ' ' -f 2-)
  [[ $branch ]] && branch=" (git:$branch)"
  printf "%s" "$branch"
}
ssh-ps1() {
  [[ $SSH_CONNECTION ]] && printf "%s" "($HOSTNAME) "
}
# ANSI color codes
RS="\[\033[0m\]"    # reset
FCYN="\[\033[36m\]" # foreground cyan
export PS1="$(ssh-ps1)$FCYN\w$RS\$(git-branch) ─ "

### history
export HISTSIZE=
export HISTFILESIZE=
export HISTCONTROL="ignoreboth:erasedups"

### completion
[[ -f /etc/bash_completion ]] && . /etc/bash_completion

### alias
alias ls="ls --color=auto"
alias grep="grep --color"
alias diff="diff --color=auto"
alias glog="git log --decorate --oneline --graph --all"
alias zathura="zathura --fork"
alias cpu-eater="ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%cpu | head"
alias mem-eater="ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head"

### fzf
[ -f /usr/share/doc/fzf/key-bindings.bash ] && source /usr/share/doc/fzf/key-bindings.bash

export FZF_DEFAULT_COMMAND='rg --files --hidden --follow'
export FZF_DEFAULT_OPTS="--layout=reverse"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

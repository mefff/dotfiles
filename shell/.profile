# path
PATH="$HOME/bin:$PATH"
PATH="$HOME/games/bin:$PATH"
PATH="$HOME/.cabal/bin:$PATH"
PATH="$HOME/.local/bin:$PATH"
PATH="$HOME/.ghcup/bin:$PATH"
export PATH="$HOME/.cargo/bin:$PATH"

if [ ~/.bashrc ]; then
  . ~/.bashrc
fi

# autostartx
if [ -z "$DISPLAY" ] && [ "$(fgconsole)" -eq 1 ]; then
  exec startx > /dev/null
fi

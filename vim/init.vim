" general
filetype indent plugin on
syntax on
set nocompatible
set encoding=utf8
set history=50
set hidden
set scrolloff=4
set noswapfile
set wildmenu
let mapleader = ";"
let maplocalleader = ";"

" plugins
call plug#begin('~/.local/share/nvim/plugged')

Plug 'ap/vim-buftabline'
Plug 'tpope/vim-dispatch'
Plug 'itchyny/lightline.vim'
Plug 'scrooloose/syntastic'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all'  }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-fugitive'
Plug 'rust-lang/rust.vim'
Plug 'andrejtokarcik/agda-vim'
Plug 'neovimhaskell/haskell-vim'
"Plug 'mtikekar/nvim-send-to-term'
"Plug 'mefff/agda-vim', { 'branch': 'no-leader-to-unicode' }

call plug#end()

" indent
set autoindent

" line break
set wrap
set linebreak
set breakindent
set breakindentopt=min:40

" backspace
set backspace=indent,eol,start

" tabs
set expandtab
set softtabstop=0
set shiftwidth=2
set tabstop=2
set shiftround

" search
set nohlsearch
set incsearch

" ui
set noruler
set number relativenumber
set nonumber norelativenumber  " turn hybrid line numbers off
set number relativenumber      " toggle hybrid line numbers
set mouse=a
set shortmess=atI

" theme
set laststatus=2 " set to 2 to show always
set noshowmode " hide -- INSERT --
colorscheme cscheme

" bindings
nnoremap <C-N> :bnext<CR>
nnoremap <C-B> :bprev<CR>
nnoremap <C-T> :bo 15sp +te<CR>
nnoremap <Leader>m :Make!<CR>

" fzf
" --column: Show column number
" --line-number: Show line number
" --no-heading: Do not show file headings in results
" --fixed-strings: Search term as a literal string
" --ignore-case: Case insensitive search
" --no-ignore: Do not respect .gitignore, etc...
" --hidden: Search hidden files and folders
" --follow: Follow symlinks
" --glob: Additional conditions for search (in this case ignore everything in the .git/ folder)
" --color: Search color options

command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>), 1, <bang>0)
nnoremap <C-F> :Find<CR>

" delete trailing spaces when save
autocmd BufWritePre * %s/\s\+$//e

" kill all term buffers
nmap ;q :bd! term://<CR>

" run with python3
autocmd Filetype python nmap <buffer> ;r :!python3 %<CR>
autocmd Filetype python nmap <buffer> ;i :bo 6sp<CR>:terminal python3 -i %<CR>

" run with cabal
autocmd FileType haskell nmap <buffer> ;r :!cabal run<CR>
autocmd FileType haskell set makeprg=cabal\ build

" terminal emulator
tnoremap <Esc> <C-\><C-n>

" Move between windows
tnoremap <A-h> <C-\><C-n><C-w>h
tnoremap <A-j> <C-\><C-n><C-w>j
tnoremap <A-k> <C-\><C-n><C-w>k
tnoremap <A-l> <C-\><C-n><C-w>l
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" agda-mode
let g:agdavim_enable_goto_definition = 0 " to try to speed up things

" tex
au FileType tex set makeprg=pdflatex\ \%
au FileType tex imap ;a á
au FileType tex imap ;e é
au FileType tex imap ;i í
au FileType tex imap ;o ó
au FileType tex imap ;u ú
au FileType tex imap ;n ñ
au Filetype tex set spelllang=es
"au Filetype tex set spell
au Filetype tex noremap <Leader>b cw\begin{<C-R>"}<CR>\end{<C-R>"}<ESC>ko

" haskell-vim
let g:haskell_classic_highlighting = 1
